# py-random-walk
Simple random walk example in python with numpy and matplotlib

```
$ virtualevn -p /usr/bin/python3 venv
$ . venv/bin/activate
(venv) $ pip install numpy
(venv) $ pip install matplotlib
(venv) $ python rw_visual.py
```

Example basically verbatim from 'Python Crash Course'.
